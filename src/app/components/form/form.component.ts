import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  form!: FormGroup;
  content: string[] = []

  constructor(private fb: FormBuilder) { 
    this.crearFormulario();
  }

  crearFormulario(): void{
    this.form = this.fb.group({
      cajaG: [''],
      cajaP: this.fb.array([[]])
    })
  }

  ngOnInit(): void {
  }

  get contenidoP(){
    return this.form.get('cajaP') as FormArray
  }

  agregar():void{
    this.contenidoP.push(this.fb.control('', Validators.required))
  }
  borrarCaja(i: number): void{
    this.contenidoP.removeAt(i);
  }
  limpiarCajaP(i: number){
    this.form.value.cajaP.reset
  }

  limpiar(): void {
    this.content = ['']
    this.form.reset;
  }
  limpiarCajaG(): void{
    this.content = ['']
  }

  guardar(): void{
    console.log('guardar');
    this.content = this.form.value.cajaP
  }

}
